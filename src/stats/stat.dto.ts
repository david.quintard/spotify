import { IsInt, IsString } from 'class-validator';
import {ApiProperty} from "@nestjs/swagger";

export class StatDto {
  @ApiProperty()
  @IsString()
  readonly date: string;

  @ApiProperty()
  @IsString()
  readonly indicator: 'songs' | 'popularity' | 'duration' | 'mode' | 'danceability' | 'energy' | 'valence' | 'tempo';

  @ApiProperty()
  @IsString()
  readonly order: 'ASC' | 'DESC';
}
