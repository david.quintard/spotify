import { Injectable } from '@nestjs/common';
import {Column, Repository} from "typeorm";
import {InjectRepository} from "@nestjs/typeorm";
import {Stat} from "./stat.entity";
import {Artist} from "../artists/artist.entity";
import {StatDto} from "./stat.dto";


@Injectable()
export class StatsService {
  constructor(
    @InjectRepository(Stat) private readonly statRepository: Repository<Stat>
  ) {}

  public async build(artist: Artist, albums: any[]) {

    let songCounter = 0;
    let popularitySum = 0;
    let durationSum = 0;
    let keys = [0,1,2,3,4,5,6,7,8,9,10,11];
    let modeSum = 0;;
    let danceabilitySum = 0;
    let energySum = 0;
    let valenceSum = 0;
    let tempoSum = 0;

    for(const album of albums) {
      for(const track of album.tracks) {
        popularitySum += track.popularity;

        const audio = track.audio;
        durationSum += audio.duration_ms;
        keys[audio.key] += 1;
        modeSum += audio.mode;
        danceabilitySum += audio.danceability;
        energySum += audio.energy;
        valenceSum += audio.valence
        tempoSum += audio.tempo;

        songCounter++;
      }
    }

    const today = new Date();
    const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

    let stat = await this.statRepository.findOne({
      artist: artist,
      date: date
    });
    if(!stat) {
      stat = new Stat();
      stat.date = date;
      stat.artist = artist;
      stat.songs = songCounter;
      stat.popularity = Math.round(popularitySum / songCounter);
      stat.duration = Math.round(durationSum / songCounter / 1000);
      stat.mode = Math.round(100 * modeSum / songCounter);
      stat.danceability = Math.round(100 * danceabilitySum / songCounter);
      stat.energy = Math.round(100 * energySum / songCounter);
      stat.valence = Math.round(100 * valenceSum / songCounter);
      stat.tempo = Math.round(tempoSum / songCounter);
      stat.keyMain = '@TODO';
      stat.key0 = Math.round(100 * keys[0] / songCounter);
      stat.key1 = Math.round(100 * keys[1] / songCounter);
      stat.key2 = Math.round(100 * keys[2] / songCounter);
      stat.key3 = Math.round(100 * keys[3] / songCounter);
      stat.key4 = Math.round(100 * keys[4] / songCounter);
      stat.key5 = Math.round(100 * keys[5] / songCounter);
      stat.key6 = Math.round(100 * keys[6] / songCounter);
      stat.key7 = Math.round(100 * keys[7] / songCounter);
      stat.key8 = Math.round(100 * keys[8] / songCounter);
      stat.key9 = Math.round(100 * keys[9] / songCounter);
      stat.key10 = Math.round(100 * keys[10] / songCounter);
      stat.key11 = Math.round(100 * keys[11] / songCounter);

      await this.statRepository.save(stat);
    }
    return stat;
  }

  public async show(statDto: StatDto) {
    const indicator = 'stat.' + statDto.indicator;
    let stats = await this.statRepository.createQueryBuilder('stat')
      .innerJoinAndSelect('stat.artist', 'artist')
      .where('stat.date = :date', {date: statDto.date})
      .orderBy(indicator, statDto.order)
      .getMany();
    return stats;
  }

  private getKeyName(value: number) {
    const keys = {
      0: 'C',
      1: 'C#',
      2: 'D',
      3: 'E♭',
      4: 'E',
      5: 'F',
      6: 'F#',
      7: 'G',
      8: 'G#',
      9: 'A',
      10: 'B♭',
      11: 'B'
    }
    return keys[value];
  }
}
