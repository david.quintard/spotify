import { Module } from '@nestjs/common';
import {TypeOrmModule} from "@nestjs/typeorm";
import { StatsController } from './stats.controller';
import { StatsService } from './stats.service';
import {ArtistsModule} from "../artists/artists.module";
import {SpotifyService} from "../spotify/spotify.service";
import { Stat } from './stat.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Stat]), ArtistsModule],
  controllers: [StatsController],
  providers: [StatsService, SpotifyService]
})
export class StatsModule {}
