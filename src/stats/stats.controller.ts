import {Body, Controller, Get, HttpException, Post, Query, ValidationPipe} from '@nestjs/common';
import {ApiTags, ApiOperation} from "@nestjs/swagger";
import {StatsService} from "./stats.service";
import {ArtistsService} from "../artists/artists.service";
import {SpotifyService} from "../spotify/spotify.service";
import {StatDto} from "./stat.dto";

@ApiTags('stat')
@Controller('stats')
export class StatsController {
  constructor(
    private readonly statsService: StatsService,
    private readonly artistsService: ArtistsService,
    private readonly spotifyService: SpotifyService,

  ) {}

  @ApiOperation({
    summary: 'Recherche d\'artiste dans le référentiel Spotify',
    description: 'Recherche d\'artiste dans le référentiel Spotify et sauvegarde en local des informations relatives aux albums et tracks de l\'artiste.',
  })
  @Get('/spotify')
  async getSpotifyStats(
    @Query('term') term: string,
  ): Promise<any> {
    try {
      const artist = await this.spotifyService.searchArtist(term);
      const artistEntity = await this.artistsService.save(artist);
      const albums = await this.spotifyService.getStats(artist.id);
      return await this.statsService.build(artistEntity, albums);
    } catch(error) {
      throw new HttpException(
        error.message,
        error.status,
      );
    }
  }

  @ApiOperation({
    summary: 'Restitutions des statistiques en local',
    description: 'Restitution des statistiques pour une date données, et pour un indicateur donné (songs | popularity | duration | mode | danceability | energy | valence | tempo).<br>Le tri doit être (ASC | DESC) ',
  })
  @Post('/')
  async getStats(
    @Body(new ValidationPipe()) body: StatDto,
  ): Promise<any> {
    try {
      return await this.statsService.show(body);
    } catch(error) {
      throw new HttpException(
        error.message,
        error.status,
      );
    }
  }
}
