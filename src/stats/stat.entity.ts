import {Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToOne} from 'typeorm';
import {Artist} from "../artists/artist.entity";

@Entity()
export class Stat {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 10 })
  date: string;

  @ManyToOne(type => Artist, artist => artist.stats)
  artist: Artist;

  @Column()
  songs: number;

  @Column("float")
  popularity: number;

  @Column("float")
  duration: number;

  @Column("float")
  mode: number;

  @Column("float")
  danceability: number;

  @Column("float")
  energy: number;

  @Column("float")
  valence: number;

  @Column("float")
  tempo: number;

  @Column()
  keyMain: string;

  @Column()
  key0: number;

  @Column()
  key1: number;

  @Column()
  key2: number;

  @Column()
  key3: number;

  @Column()
  key4: number;

  @Column()
  key5: number;

  @Column()
  key6: number;

  @Column()
  key7: number;

  @Column()
  key8: number;

  @Column()
  key9: number;

  @Column()
  key10: number;

  @Column()
  key11: number;
}
