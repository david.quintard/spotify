import {Body, Controller, Get, HttpException, Post, Query, ValidationPipe} from '@nestjs/common';
import {ApiOperation, ApiTags} from "@nestjs/swagger";
import {SpotifyService} from "../spotify/spotify.service";
import {ArtistsService} from "./artists.service";
import {ArtistDto} from "./artist.dto";

@ApiTags('artist')
@Controller('artists')
export class ArtistsController {
  constructor(
    private readonly spotifyService: SpotifyService,
    private readonly artistsService: ArtistsService
  ) {}

  @ApiOperation({
    summary: 'Recherche d\'artiste dans le référentiel Spotify',
    description: 'Recherche d\'artiste dans le référentiel Spotify',
  })
  @Get('/spotify')
  async searchSpotifyArtist(
    @Query('term') term: string,
  ): Promise<any> {
    try {
      return await this.spotifyService.searchArtist(term);
    } catch(error) {
      throw new HttpException(
        error.message,
        error.status,
      );
    }
  }

  @ApiOperation({
    summary: 'Sauvegarde en local d\'un artiste',
    description: 'Sauvegarde en local d\'un artiste',
  })
  @Post('/')
  async saveArtist(
    @Body(new ValidationPipe()) body: ArtistDto,
  ): Promise<any> {
    try {
      return await this.artistsService.save(body);
    } catch(error) {
      throw new HttpException(
        error.message,
        error.status,
      );
    }
  }
}
