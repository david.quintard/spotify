import {Entity, Column, PrimaryColumn, OneToMany} from 'typeorm';
import {Stat} from "../stats/stat.entity";

@Entity()
export class Artist {
  @PrimaryColumn()
  id: string;

  @Column()
  name: string;

  @Column()
  popularity: number;

  @Column()
  followers: number;

  @OneToMany(type => Stat, stat => stat.artist)
  stats: Stat[];
}
