import { IsInt, IsString, IsOptional } from 'class-validator';
import {ApiProperty} from "@nestjs/swagger";

export class ArtistDto {
  @ApiProperty()
  @IsString()
  readonly id: string;

  @ApiProperty()
  @IsString()
  readonly name: string;

  @ApiProperty()
  @IsOptional()
  @IsInt()
  readonly popularity: number;

  @ApiProperty()
  @IsOptional()
  @IsInt()
  readonly followers: number;
}
