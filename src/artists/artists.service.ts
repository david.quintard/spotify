import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Artist } from './artist.entity';
import {ArtistDto} from "./artist.dto";

@Injectable()
export class ArtistsService {
  constructor(
    @InjectRepository(Artist) private readonly artistRepository: Repository<Artist>
  ) {}

  public async save(artistDto: ArtistDto) {
    let artist = await this.artistRepository.findOne({id: artistDto.id});
    if(!artist) {
      artist = new Artist();
      artist.id = artistDto.id;
      artist.name = artistDto.name;
    }
    artist.popularity = artistDto.popularity;
    artist.followers = artistDto.followers;

    return await this.artistRepository.save(artist);
  }
}
