import { Module } from '@nestjs/common';
import {TypeOrmModule} from "@nestjs/typeorm";
import { ArtistsController } from './artists.controller';
import { ArtistsService } from './artists.service';
import {SpotifyService} from "../spotify/spotify.service";
import { Artist } from './artist.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Artist])],
  controllers: [ArtistsController],
  providers: [ArtistsService, SpotifyService],
  exports: [ArtistsService]
})
export class ArtistsModule {}
