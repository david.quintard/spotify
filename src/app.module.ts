import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import {TypeOrmModule} from "@nestjs/typeorm";
// import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ArtistsModule } from './artists/artists.module';
import { StatsModule } from './stats/stats.module';
import { SpotifyService } from './spotify/spotify.service';


@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot(),
    ArtistsModule,
    StatsModule
  ],
  controllers: [], // AppController
  providers: [AppService, SpotifyService],
})
export class AppModule {}
