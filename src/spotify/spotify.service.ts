import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import axios, {AxiosResponse} from 'axios';
const qs = require('qs');

@Injectable()
export class SpotifyService {
  private token: string;
  private async getToken(): Promise<any> {
    let credentials = process.env.SPOTIFY_CLIENT_ID + ':' + process.env.SPOTIFY_CLIENT_SECRET;
    let buff = new Buffer(credentials);

    const response: AxiosResponse = await axios.post(
      process.env.SPOTIFY_ACCOUNT_ENDPOINT,
      qs.stringify({grant_type: 'client_credentials'}),
      {
        headers: {
          Authorization: 'Basic ' + buff.toString('base64'),
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }
    );
    return response.data.access_token;
  }

  public async searchArtist(term: string): Promise<any> {
    const token = await this.getToken();
    const headers = {
      Authorization: 'Bearer ' + token
    };

    const response: AxiosResponse = await axios.get(
      process.env.SPOTIFY_API_ENDPOINT + '/search?type=artist&q=' + encodeURI(term),
      { headers: { Authorization: 'Bearer ' + token }}
    );
    // recherche des artistes dont le nom correspond exactement à la recherche
    const artists = response.data.artists.items.filter(function(item) {
      return item.name.toLowerCase() == term.toLowerCase()
    });

    if(artists.length === 0) {
      throw new HttpException(
        'Aucun artiste trouvé!',
        HttpStatus.NOT_FOUND,
      );
    }

    // On prend le 1er artiste de la liste (plus haute popularité)
    const artist = artists[0];
    return {
      id: artist.id,
      name: artist.name,
      popularity: artist.popularity,
      followers: artist.followers.total,
    }
  }

  // Transforme un tableau de n elements en un tableau de listes de 20 éléments max
  private getGroupList(list) {
    const groups = [];
    let j = 0;
    groups[j] = [];
    for(let i=0;i<list.length;i++) {
      const item = list[i];
      const itemId = item.id;
      if(groups[j].length === 20) {
        j++;
        groups[j] = [];
      }
      groups[j].push(itemId);
    }
    for(let i=0;i<groups.length;i++) {
      groups[i] = groups[i].join()
    }
    return groups;
  }

  public async getStats(artisteId: string): Promise<any> {
    this.token = await this.getToken();

    const stats = [];
    const albumsList = await this.getAlbumsByArtist(artisteId);
    // construction de groupes de 20 albums
    const albumsGroup = this.getGroupList(albumsList)
    for(const albumIds of albumsGroup) {
      const albums = await this.getAlbums(albumIds);
      for(const album of albums) {
        // Initialisation des stats d'un album
        const stat = {
          id : album.id,
          name: album.name,
          popularity: album.popularity,
          genres: album.genres,
          tracks: []
        }
        // construction de groupes de 20 tracks
        const tracksGroup = this.getGroupList(album.tracks.items);
        for(const trackIds of tracksGroup) {
          const audioFeatures = await this.getAudioFeatures(trackIds);
          const tracks = await this.getTracks(trackIds);
          for(const track of tracks) {
            // recherche des features audio de la track courante
            const audioFeature = audioFeatures.find(function(item) {
              return item.id == track.id
            });

            stat.tracks.push({
              id: track.id,
              name: track.name,
              popularity: track.popularity,
              audio: {
                duration_ms: audioFeature.duration_ms,
                key: audioFeature.key,
                mode: audioFeature.mode,
                acousticness: audioFeature.acousticness,
                danceability: audioFeature.danceability,
                energy: audioFeature.energy,
                instrumentalness: audioFeature.instrumentalness,
                loudness: audioFeature.loudness,
                valence: audioFeature.valence,
                tempo: audioFeature.tempo
              }
            })
          }
        }
        stats.push(stat);
      }
    }
    return stats;
  }

  private async getAlbumsByArtist(artisteId: string): Promise<any> {
    let count = 0;
    let offset = 0;
    let albums = [];
    // Recherche itérative si l'artiste a plus de 50 albums
    do {
      const response: AxiosResponse = await axios.get(
        process.env.SPOTIFY_API_ENDPOINT + '/artists/' + artisteId + '/albums?country=FR&offset=' + offset + '&limit=50&include_groups=album',
        {headers: {Authorization: 'Bearer ' + this.token}});
      albums = albums.concat(response.data.items);
      count = response.data.items.length;
      offset += 50;
    } while (count === 50);
    return albums;
  }

  private async getAlbums(albumIds: string): Promise<any> {
    const response: AxiosResponse = await axios.get(
      process.env.SPOTIFY_API_ENDPOINT + '/albums?ids=' + albumIds,
      {headers : {Authorization: 'Bearer ' + this.token}}
    );
    return response.data.albums;
  }

  private async getTracks(trackIds: string): Promise<any> {
    const response: AxiosResponse = await axios.get(
      process.env.SPOTIFY_API_ENDPOINT + '/tracks?ids=' + trackIds,
      {headers: {Authorization: 'Bearer ' + this.token}});
    return response.data.tracks;
  }

  private async getAudioFeatures(trackIds: string): Promise<any> {
    const response: AxiosResponse = await axios.get(
      process.env.SPOTIFY_API_ENDPOINT + '/audio-features?ids=' + trackIds,
      {headers: {Authorization: 'Bearer ' + this.token}});
    return response.data.audio_features;
  }
}
