import {MigrationInterface, QueryRunner} from "typeorm";

export class Initialisation1593259996634 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `artist` (`id` CHAR(30) NOT NULL, `name` VARCHAR(50) NOT NULL DEFAULT '', `popularity` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0, `followers` INT(10) UNSIGNED NOT NULL DEFAULT 0, PRIMARY KEY (`id`))");
        await queryRunner.query("CREATE TABLE `stat` (`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT, `date` DATE NOT NULL, `artistId` CHAR(30) NOT NULL DEFAULT '', `songs` SMALLINT(6) NOT NULL DEFAULT 0, `popularity` FLOAT NOT NULL DEFAULT 0, `duration` FLOAT NULL DEFAULT NULL, `mode` FLOAT NULL DEFAULT NULL,	`danceability` FLOAT NULL DEFAULT NULL,	`energy` FLOAT NULL DEFAULT NULL, `valence` FLOAT NULL DEFAULT NULL, `tempo` FLOAT NULL DEFAULT NULL,	`keyMain` CHAR(5) NULL DEFAULT NULL, `key0` SMALLINT(6) NOT NULL DEFAULT 0, `key1` SMALLINT(6) NOT NULL DEFAULT 0, `key2` SMALLINT(6) NOT NULL DEFAULT 0, `key3` SMALLINT(6) NOT NULL DEFAULT 0, `key4` SMALLINT(6) NOT NULL DEFAULT 0, `key5` SMALLINT(6) NOT NULL DEFAULT 0, `key6` SMALLINT(6) NOT NULL DEFAULT 0, `key7` SMALLINT(6) NOT NULL DEFAULT 0, `key8` SMALLINT(6) NOT NULL DEFAULT 0, `key9` SMALLINT(6) NOT NULL DEFAULT 0, `key10` SMALLINT(6) NOT NULL DEFAULT 0, `key11` SMALLINT(6) NOT NULL DEFAULT 0, PRIMARY KEY (`id`),	INDEX `FK_stat_artist` (`artistId`), INDEX `date` (`date`), CONSTRAINT `FK_stat_artist` FOREIGN KEY (`artistId`) REFERENCES `artist` (`id`))");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("DROP TABLE `stat`");
        await queryRunner.query("DROP TABLE `artist`");
    }

}
