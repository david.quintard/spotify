module.exports = {
    type: 'mariadb',
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: 'admin',
    database: 'spotify',
    entities: ['src/**/**.entity{.ts,.js}'],
    synchronize: false,
    migrationsTableName: "_migration",
    migrations: ["migration/*.ts"],
    cli: {
        migrationsDir: "migration"
    }
}
