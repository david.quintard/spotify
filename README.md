## Description

APIs de recherche / sauvegarde et restitution des données Spotify.

### Prérequis

Il est nécessaire de créer une base de données mysql ou mariadb en local.

ex: CREATE DATABASE spotify
 
## Récupération du projet

```bash
$ git clone https://gitlab.com/david.quintard/spotify.git
```

## Configuration

Modifier les paramètres de connexion à votre base dans ormconfig.js.<br>
Par défaut les valeurs sont les suivantes:
- host: 'localhost',
- port: 3306,
- username: 'root',
- password: 'admin',
- database: 'spotify',

## Installation

```bash
$ npm install
```

## Démarrage du serveur

```bash
$ npm run start
```

## Tester les APIs

Ouvrir votre navigateur et aller à :
```bash
http://localhost:3000
```
